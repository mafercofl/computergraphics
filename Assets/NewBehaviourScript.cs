﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour {

    Renderer cubito;
    float slider = 0f;

	// Use this for initialization
	void Start () {
        cubito = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
        cubito.material.SetFloat("_MySlider", slider++ * Time.deltaTime);
        //cubito.material.SetColor("_AmbientColor", new Vector4(0, 1, 0, 1));
    }

    /*IEnumerator SwapSH()
    {
        cubito.material.SetColor("_AmbientColor", new Vector4(0, 0, 1, 0));
        yield return new WaitForSeconds(5);
        cubito.material.SetColor("_AmbientColor", new Vector4(0, 0, 1, 1));
    }*/
}
