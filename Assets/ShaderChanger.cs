﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderChanger : MonoBehaviour {

    public static Renderer sphere;
    float slider = 0f;

	// Use this for initialization
	void Start () {
        sphere = GetComponent<Renderer>();
        sphere.material.SetFloat("_MySlider", 10f);
        StartCoroutine(SwitchInTime());
	}
	
	// Update is called once per frame
	void Update () {
        //StartCoroutine(SwitchInTime());
        //sphere.material.shader = Shader.Find("Custom/CustomShader");
        //sphere.material.SetFloat("_MySlider", slider+=.1f * Time.deltaTime);
    }

    IEnumerator SwitchInTime()
    {
        // 1 - PLANO

        // 2 - PLANO + NEGRO (Diffuse)
        sphere.material.shader = Shader.Find("Custom/CustomShader");
        sphere.material.SetFloat("_MySlider", 10);
        yield return new WaitForSeconds(5);
        // 3 - PLANO + NEGRO + LUZ (Diffuse)
        yield return new WaitForSeconds(5);
        // 4 - PLANO + COLOR + LUZ (Diffuse)
        sphere.material.shader = Shader.Find("Custom/CustomShader");
        sphere.material.SetColor("_Color", new Vector4(1, 0, 0, 1));
        //while(slider > 1)
        {
            //sphere.material.SetFloat("_MySlider", slider-- * Time.deltaTime);
        }
        yield return new WaitForSeconds(5);
        // 5 - TEXTURA + LUZ (Textured)
        sphere.material.shader = Shader.Find("Custom/TexturedShader");
        yield return new WaitForSeconds(5);
        // 6 - VOLUMEN + LUZ (Normal Map)
        sphere.material.shader = Shader.Find("Custom/NormalMapShader");
        yield return new WaitForSeconds(5);
        // 7 - TEXTURA + VOLUMEN + LUZ  (Tetured + Normal Map)
        sphere.material.shader = Shader.Find("Custom/TexturedNormalMapShader");
        yield return new WaitForSeconds(5);
        //8 - ZOOM IN
    }
}
