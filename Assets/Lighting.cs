﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lighting : MonoBehaviour {

    public static Light sol;

	// Use this for initialization
	void Start () {
        sol = GetComponent<Light>();
        sol.enabled = false;
        StartCoroutine(TurnOn());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator TurnOn()
    {
        yield return new WaitForSeconds(3);
        sol.enabled = true;
    }
}
