﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma : MonoBehaviour {

	// Use this for initialization
	void Start () {
        gameObject.SetActive(false);
        StartCoroutine(Sombras());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator Sombras()
    {
        yield return new WaitForSeconds(10);
        gameObject.SetActive(true);
    }
}
