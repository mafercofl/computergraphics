﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Texto : MonoBehaviour {

    public Text timer;
    private float currentTime = 0;

    // Use this for initialization
    void Start () {
        timer = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        timer.text = "Shader: " + ShaderChanger.sphere.material.shader.name.ToString() + "\n" +
            "Light: " + Lighting.sol.enabled.ToString() + "\n" +
            "Color: " + ShaderChanger.sphere.material.color.ToString() + "\n" +
            //"Texture: " + ShaderChanger.sphere.material.mainTexture.filterMode.ToString() + "\n" +
            "Time: " + (currentTime = Time.time).ToString();
	}
}
